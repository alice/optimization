const vw = 700
const vh = 700
const xrange = [-4, 4]
const yrange = [-5, 5]
const xunit = (xrange[1] - xrange[0]) / vw
const yunit = (yrange[1] - yrange[0]) / vh



// our objective function and its first-order derivative

// const loss = x => x * (x - 2) * (x + 3) * (x**2 - 2) * 0.1
// const dloss = x => (5 * (x ** 4) + 4 * (x ** 3) - 24 * (x ** 2) - 4 * x + 12) * 0.1


const loss = x => 0.2 * (x + 1) * (3*x - 1) * (x + 2) * (x - 2)
const dloss = x => (loss(x+0.01) - loss(x)) / 0.01 // approximate gradient


// maps coordinates on a graph to locations on screen

const coords = (x, y) => [
  ((x - xrange[0]) / (xrange[1] - xrange[0])) * vw,
  ((y - yrange[0]) / (yrange[1] - yrange[0])) * vh
]


// picks a random value on a given interval

const randrange = (start, stop) => start + Math.random() * (stop - start)



// plots the objective function 



function plot() {
  
  let [ xstart, xstop ] = xrange
  let [ ystart, ystop ] = yrange
  
  for (let i = 0, x = xstart; x < xstop; x += xunit) {
    
    let y0 = coords(0, loss(x))[1]
    let y1 = coords(0, loss(x + xunit))[1]
    
    line(i, y0, ++i, y1)
  }
}



// steepest ascent hill climbing

function *doHillClimbing(x) {
  let current = x
  
  while (1) {
    let here = loss(current)
    let left = loss(current - xunit)
    let right = loss(current + xunit)
    
    let best = Math.max(here, left, right)
    
    if (best == here) {
      return
    } else if (best == left) {
      current += xunit
    } else {
      current -= xunit
    }
      
    yield current
  }
}



// gradient ascent in two dimensions. (a) is the learning rate


function *doGradientAscent(x, a=xunit, t=600) {
  
  let current = x
  let i = 0
  
  while (i++ < t) {
    let m = dloss(current)
    current -= m * a
    
    yield current
  }
}

  
  
// tornament selection and crossover

function *doEvolution(n=200, t=100) {
  
  // generate an initial population

  let population = Array(n).fill(0).map(() => randrange(...xrange))

  let k = n / 4 // tornament size
  let p = 1
  let m = 0.1 // mutation rate
  let ma = 10 * xunit // mutation amount
  
  for (let g=0; g < t; g++) {
    // tornament selection with k candidates

    let winners = []
    let candidates = population.slice(0, k).sort((a, b) => loss(a) > loss(b) ? 1 : -1)
    
    // yield the median of this tornament (so we can see progress)
    
    yield candidates[Math.floor(candidates.length / 2)]

    
    candidates.forEach((candidate, rank) => {
      if (Math.random() < (p / Math.sqrt(rank))) winners.push(candidate)
    })
    
    
    // create k children using the winners as parents
    
    let children = Array(k).fill(0).map(() => {
      let p1 = Math.floor(randrange(0, winners.length))
      let p2 = Math.floor(randrange(0, winners.length))
      
      // crossover using a simple mean and add some mutation
      
      let value = (winners[p1] + winners[p2]) / 2
      
      if (Math.random() < m) {
        value += randrange(-ma, ma)
      }
      
      return value
    })

    
    // add the children into the population and shuffle
    
    population = [...children, ...population.slice(k)]
    population = population.sort(() => randrange(-1, 1))
    
    // next generation...
  }
  
}
  


// canvas setup
  
function setup() {
  createCanvas(vw, vh);
  background(255);
  strokeWeight(2)
  
  plot()
  
  stroke(255, 0, 0)
  fill(255, 0, 0)
}


  
// main loop

let solution1 = doGradientAscent(randrange(...xrange))
let solution2 = doHillClimbing(randrange(...xrange))
let solution3 = doEvolution()



function draw() {
  let x = solution3.next().value
  let y = loss(x)
  
  circle(...coords(x, y), 3)
}